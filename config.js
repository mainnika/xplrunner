module.exports = {
	exploits_dir: '/tmp/xpl',	// Exp dir
	sync: false,			// Xecute next exploit only if prev finished, unstable!
	interval: 3,			// seconds between hack
	timeout: 5,			// wait seconds, and SIGKILL exploit
	log: {
		File: {
			handleExceptions: true,
			timestamp: true,
			filename: 'xplrunner.log',
			json: true
		},
		Console: {
			handleExceptions: true,
			timestamp: false,
			json: false,
			colorize: true
		}
	},
	flagger: {
		host: '127.0.0.1',
		port: 2222
	},
	targets: [
		'127.0.0.1',
		'127.0.0.2'
	]
};
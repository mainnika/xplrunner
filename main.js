/*
 * Exploit runner, 
 * mainnika, 
 * uctf 2014
*/

var 
	net = require('net'),
	async = require('async'),
	cluster = require('cluster'),
	config = require('./config'),
	logger = require('./logger').Logger('MAIN'),
	sender = require('./sender'),
	Engine = require('./engine');


if (cluster.isMaster) {
	// Fork workers.
	for (var i = 0; i < config.targets.length; i++) {
		logger.info('Forking...');
		cluster.fork();
	}

	cluster.on('online', function(worker, code, signal) {
		logger.info('Worker ' + worker.process.pid + ' started');
	});
	cluster.on('exit', function(worker, code, signal) {
		logger.warn('Worker ' + worker.process.pid + ' died');
	});
} else {

	var target = config.targets[cluster.worker.id-1];

	logger.info('Creating engine for target %s', target);
	var engine = new Engine(config.exploits_dir, config.timeout, target, config.sync);

	var run = function(){

		logger.info('Starting job for %s', target);

		async.series([

			function(callback){
				engine.update_list(callback);
			},

			function(callback){
				engine.run(callback);
			}

		], function(err,result){

			sender.send_bufs(result[1]);

		})
	}

	run();

	setInterval(run, config.interval*1000);

}

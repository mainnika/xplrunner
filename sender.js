var
	net = require('net'),
	async = require('async'),
	config = require('./config'),
	logger = require('./logger').Logger('SENDER');

exports.send_bufs = function(bufs, callback){

	logger.info('Sending data to fragger...');

	var sock = net.connect(config.flagger);

	async.series([
		function(callback){
			logger.info('Connecting to %s:%s...', config.flagger.host, config.flagger.port);
			sock.on('connect', callback);
			sock.on('error', callback);
		},
		function(callback){
			logger.info('Sending to %s:%s...', config.flagger.host, config.flagger.port);
			async.map(bufs, function(one,callback){sock.write(one,null,callback)}, callback);
		},
		function(callback){
			logger.info('Finalize connection %s:%s...', config.flagger.host, config.flagger.port);
			sock.end();
			callback();
		}
	], function(err){
		if (!err)
			return logger.info('OK');

		logger.error('Connection error: %s', err);
	})
}

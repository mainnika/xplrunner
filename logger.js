var
	winston = require('winston'),
	config = require('./config');

exports.Logger = function(name){

	var logger = new winston.Logger({
		transports: (function(){
			var transports = [];

			for (var name in config.log)
				transports.push(new winston.transports[name](config.log[name]));

			return transports;
		})(),
		exitOnError: false
	});

	var loglevels = ['info', 'warn', 'error'];
	var object = {};

	logger.info('New winston instance [%s]', name);

	loglevels.forEach(function(element){
		object[element] = function(){
			winston.Logger.prototype.log.apply(
				logger,
				[element, "["+name+"]: "+arguments[0]].concat(Array.prototype.slice.call(arguments, 1))
			);
		}
	});

	return object;
}
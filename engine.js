var
	fs = require('fs'),
	spawner = require('child_process'),
	async = require('async'),
	path = require('path'),
	logger = require('./logger').Logger("ENGINE");


var Xplr = function(dir, timeout, target, sync){
	this._x = [];
	this._xdir = dir;
	this._sync = sync;
	this._target = target;
	this._timeout = timeout;
}

Xplr.prototype.update_list = function(callback) {

	var self = this;

	logger.info('Update xpl list...');

	async.waterfall([

		function(callback){
			logger.info('Scanning directory...');

			fs.readdir(self._xdir, function(err,result){
				if (err)
					return callback(err);

				callback(null, result.map(function(filename){
					return path.join(self._xdir, filename);
				}));
			});
		},

		function(files, callback){
			logger.info('Excluding directories...');

			async.map(files, fs.stat, function(err,result){
				if (err)
					return callback(err);

				callback(null, files.map(function(filename, index){
					return result[index].isFile()?filename:null;
				}));
			})
		}

	], function(err,result){
		if (err){
			logger.error('Updating error: %s', err);
			return (callback)?callback(err):undefined;
		}
		
		self._x = result;
		
		logger.info('Updating OK, exploits count: '+self._x.length);
		
		return (callback)?callback(null, result):undefined;
	})

};

Xplr.prototype.run = function(callback){
	var self = this,

	// sync or async, it's a short way
		conveyor = (self._sync)?async.mapSeries:async.map,

	// this function execute an exploit script
		executor = function(exploit, callback){

			var outputs = [], killed = false;
			var ip = self._target;

			// spawn a detached process
			logger.info('Spawning %s %s',exploit, ip);
			var child = spawner.spawn(exploit, [ip], {
				detached: true,
				// stdin ingored, stdout standart, ,stderr in terminal
				stdio: [ 'ignore', null, 'pipe' ],
				env: process.env
			});

			// timeout for kill
			var timeout = setTimeout(function(){
				logger.error('Exploit '+exploit+' '+ip+' is timeout, i will kill him for you');
				child.kill();
			}, self._timeout*1000);

			// collect output data
			child.stdout.on('data', function(buf){
				outputs.push(buf);
			});

			child.on('error', function(err){
				logger.warn('Spawning error: %s', err);
				callback(null, '');
			})

			// if process closed
			child.on('exit', function(code, signal){
				async.series([
					function(callback){

						// if process closed very fast, node had no time for getting stdout.
						// we wait 100 ms
						if (outputs.length>0){
							callback();
						}else{
							logger.info('Output is empty, waiting 100ms for something');
							setTimeout(callback, 100);
						}

					}, function(callback){
						if (signal)
							logger.error('Exploit '+exploit+' '+ip+' killed by '+signal);
						clearTimeout(timeout);
						var output = Buffer.concat(outputs);
						logger.info('Exploit '+exploit+' '+ip+' returned '+output.length+' bytes');
						callback(null, output);
					}
				], function(err,result){
					callback(null, result[1]);
				})
			})
		},

	// this function
		finalyzer = function(err, results){

			var out = [];
			results.forEach(function(item){
				out = out.concat(item)
			});

			//console.log(results);
			callback(err, out);
		};

	conveyor(self._x,executor,finalyzer);
};

module.exports = Xplr;